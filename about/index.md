---
layout: layouts/about.njk
title: About Me
tags:
  - nav
navtitle: About
templateClass: tmpl-about
---

More than 5+ years of experience in Front-end Web Development, to bring the wireframes and designs to life through HTML5/CSS3 written Syntactically and Semantically based on web standard, as well as collaborating with backend developers to create stellar finished products. Building User-friendly and modern app is truly my passion using modern testing, debug tracking and automation tools.