---
title: Solving Fizz Buzz Using Javascript
date: 2018-12-21
tags:
  - post
  - algorithm
  - highlight
layout: layouts/post.njk
image: /img/posts/fizzbuzz.webp
author: 0
desc: Fizz Buzz is a common interview question where a program should print numbers from 1 to 20 but for multiples of 3 it will print Fizz instead of that number and for the multiples of five print Buzz and print FizzBuzz for the numbers which is multiple of 3 and 5 at the same time.
---

Fizz Buzz is a common interview question where a program should print numbers from 1 to 20 but for multiples of 3 it will print Fizz instead of that number and for the multiples of five print Buzz and print FizzBuzz for the numbers which is multiple of 3 and 5 at the same time.

Input : Integer number
Output :

```
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
Fizz Buzz
16
17
Fizz
19
Buzz
```

## Implementation

```javascript
function fizzbuzz(num) {
  for (var i = 1; i <= num; i++) {
    if (i % 15 == 0) console.log("FizzBuzz");
    else if (i % 3 == 0) console.log("Fizz");
    else if (i % 5 == 0) console.log("Buzz");
    else console.log(i);
  }
}
```

Main Point: Using modulus operator

The code is availalbe here : <a target="_blank" href="https://repl.it/@ahmd1/FizzBuzz-Algorithm">https://repl.it/@ahmd1/FizzBuzz-Algorithm</a>

There are so many ways to solve the Fizz Buzz problem so feel free to submit your solution with any programming language.

### Credits

Photo by <a target="_blank" title="Christina Kirschnerova" href="https://unsplash.com/@tina_96">Christina Kirschnerova</a>
