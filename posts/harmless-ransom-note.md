---
title: Implement Harmless Ransom Note
date: 2018-12-22
tags:
  - post
  - algorithm
  - highlight
  - hashtable
layout: layouts/post.njk
image: /img/posts/harmless-note.webp
author: 0
desc: The harmless ransom note is simply a note made of words cut out from a magazine text, Imagine that a kidnapper wants to write a ransom note and he doesn't want it to be handwritten and be traced back to him so he got a magazine and cuts off a whole word and use them to create an untraceable replica of his ransom note.
---

The harmless ransom note is simply a note made of words cut out from a magazine text, Imagine that a kidnapper wants to write a ransom note and he doesn't want it to be handwritten and be traced back to him so he got a magazine and cuts off a whole word and use them to create an untraceable replica of his ransom note.

Note that the words are case-sensitive and must use only whole words available in the magazine, you cannot use substrings or concatenation to create the words and cannot use repeat the words unless there is an available replica of the word.

Example :

```
ransome_note = "this is a secret note for you from a secret admirer";

magazine_text= "puerto rico is a great place you must hike far from town to find a secret waterfall that i am an admirer of but note taht it is not as hard as it seems this is my advice for you"
```

The harmless ransom note will return false because the word "secret" is used twice and it's found once in the magazine text.

Inputs : ransom note as sting, magazine text as string
Output : if the ransom note is applicable return true ortherwise return false.

## Implementation

```javascript
function HarmlessRansomNote(noteText, magazineText) {
  var noteArray = noteText.split(" ");
  var magazineArray = magazineText.split(" ");
  var isPossibleRansom = true;

  var magazineObj = {};
  magazineArray.forEach((word) => {
    if (!magazineObj[word]) magazineObj[word] = 0;
    magazineObj[word]++;
  });

  noteArray.forEach((word) => {
    if (magazineObj[word]) {
      magazineObj[word]--;
      if (magazineObj[word] < 0) {
        isPossibleRansom = false;
      }
    } else {
      isPossibleRansom = false;
    }
  });
  return isPossibleRansom;
}
```

Main Point : Using an object as a hash table.

The code is availalbe here : <a target="_blank" href="https://repl.it/@ahmd1/Harmless-Ransom-Note-Algorithm">https://repl.it/@ahmd1/Harmless-Ransom-Note-Algorithm</a>

### Credits

Photo by <a title="rawpixel" target="_blank" href="https://unsplash.com/@rawpixel">rawpixel</a>
