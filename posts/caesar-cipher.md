---
title: Caesar cipher encryption in Javascript
date: 2018-12-27
tags:
  - post
  - algorithm
layout: layouts/post.njk
image: /img/posts/caesar-cipher.webp
author: 0
desc: The caesar cipher is an encryption technique where we shift every letter in a given string by a given number for example the word Car would become Dbs if we shifted the letter by 1 and if we test the word Zoo it become 'App' notice that the letter Z shifted to A.
---

The caesar cipher is an encryption technique where we shift every letter in a given string by a given number for example the word 'Car' would become 'Dbs' if we shifted the letter by 1 and if we test the word 'Zoo' it become 'App' notice that the letter Z shifted to A.

Our code will handle negative numbers passed in and its a case-sensitive function.

Input : String , Integer number  
Output : encrypted string.

Test Cases:

```
ceasarCipher("Zoo keeper",2);
// output : Bqq mggrgt

ceasarCipher("Javascript",-1);
// output: Izuzrbqhos
```

## Implementation

```javascript
function ceasarCipher(str, num) {
  // for passing negative and large values
  num = num % 26;
  var lowerCaseString = str.toLowerCase();
  var alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
  var result = "";

  for (var i = 0; i < lowerCaseString.length; i++) {
    var currentLetter = lowerCaseString[i];
    if (currentLetter === " ") {
      result += currentLetter;
      continue;
    }

    var currentIndex = alphabet.indexOf(currentLetter);
    var shiftedIndex = currentIndex + num;
    if (shiftedIndex > 25) shiftedIndex = shiftedIndex - 26;
    if (shiftedIndex < 0) shiftedIndex = 26 + shiftedIndex;

    if (str[i] === str[i].toUpperCase()) {
      result += alphabet[shiftedIndex].toUpperCase();
    } else {
      result += alphabet[shiftedIndex];
    }
  }
  return result;
}
```

The code is availalbe here : <a target="_blank" href="https://repl.it/@ahmd1/Caesar-Cipher-iAlgorithim">https://repl.it/@ahmd1/Caesar-Cipher-iAlgorithim</a>

### Credits

Photo by <a target="_blank" title="rawpixel" href="https://unsplash.com/@rawpixel">rawpixel</a>
